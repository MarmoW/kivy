from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label
from kivy.uix.popup import Popup
from kivy_garden.graph import LinePlot


class ModbusPopup(Popup):
    
    _info_lb = None    
    def __init__(self, server_ip,server_port, **kwargs) :
        """Construtor da classe ScanPopup"""
        super().__init__(**kwargs) 
        self.ids.txt_ip.text = str(server_ip)
        self.ids.txt_porta.text = str(server_port)
    
    def setInfo(self, message):
        self._info_lb = Label(text=message)
        self.ids.layout.add_widget(self._info_lb)
        
    def clearInfo(self):
        if self._info_lb is not None:
            self.ids.layout.remove_widget(self._info_lb)
    
class ScanPopup(Popup):
    """Popup da configuração do tempo de varredura"""
    def __init__(self, scantime, **kwargs) :
        """Construtor da classe ScanPopup"""
        super().__init__(**kwargs) 
        self.ids.txt_st.text = str(scantime)
            
class DataGraphPopup(Popup):
    def __init__(self, plot_color_nivel,plot_color_vazao, **kwargs): #def __init__(self,xmax, plot_color_nivel,plot_color_vazao, **kwargs)
        super().__init__(**kwargs) 
        self.plot = LinePlot(line_width=1.5, color=plot_color_nivel)
        self.ids.graph_nivel.add_plot(self.plot)
        self.plot = LinePlot(line_width=1.5, color=plot_color_vazao)
        self.ids.graph_vazao.add_plot(self.plot)
        #self.ids.graph.xmax = xmax
        
class LabeledCheckBoxDataGraph(BoxLayout):
    pass

class HistGraphPopup(Popup):
    def __init__(self, **kwargs):
        super().__init__()
        for key, value in kwargs.get('tags').items():
            cb = LabeledCheckBoxHistGraph() 
            cb.ids.label.text = key
            cb.ids.label.color = value['color']
            cb.id = key 
            self.ids.sensores.add_widget(cb)
            
class LabeledCheckBoxHistGraph(BoxLayout):
    pass

class ControleInversorPopup(Popup):
    def __init__(self,**kwargs) :
        super().__init__(**kwargs) 
        
    def printa_tensao(self,data):
        self.ids.tensao.text = str(data)+" [V]"
        
    def printa_pot_entrada(self,data):
        self.ids.pot_entrada.text = str(data)+" [W]"
        
    def printa_corrente(self,data):
        self.ids.corrente.text = str(data)+" [A]"

class ControleMotorPopup(Popup):
    def __init__(self,**kwargs) :
        super().__init__(**kwargs) 
        
    def printa_freq_mot(self,data):
        self.ids.freq_mot.text = str(data)+" [hz]"
        
    def printa_rotacao(self,data):
        self.ids.rotacao.text = str(data)+" [RPM]"
        
    def printa_tempEstator(self,data):
        self.ids.temp_estator.text = str(data)+" [°C]"
    
    def set_freqDesejada(self,data):
        self.ids.temp_estator.text = str(data)
