import tempfile
from multiprocessing import Semaphore
from re import T
from sqlite3 import Date
from sys import get_asyncgen_hooks
from time import time
from xmlrpc.client import Boolean

from sqlalchemy import Boolean, Column, DateTime, Float, Integer

from dbalchemy import Base


class DadosCLP(Base):
    __tablename__ = 'dadosclp'
    id = Column(Integer, primary_key=True,autoincrement=True)
    timestamp = Column(DateTime)
    estado_mot = Column(Boolean)
    freq_des = Column(Float)
    t_part = Column(Float)
    freq_mot = Column(Float)
    tensao = Column(Float)
    rotacao = Column(Float)
    pot_entrada = Column(Float)
    corrente = Column(Float)
    temp_estator = Column(Float) 
    vz_entrada = Column(Float)
    nivel = Column(Float)
    nivel_h = Column(Boolean)
    nivel_l = Column(Boolean)
    Solenoide1 = Column(Boolean)
    Solenoide2 = Column(Boolean)
    Solenoide3 = Column(Boolean)

    def get_attr_printable_list(self):
        return[self.id,
        self.timestamp.strftime('%d/%m/%Y %H:%M:%S.%f'),
        self.estado_mot,
        self.freq_des,
        self.t_part,
        self.freq_mot,
        self.tensao,
        self.rotacao,
        self.pot_entrada,
        self.corrente,
        self.temp_estator,
        self.vz_entrada,
        self.nivel,
        self.nivel_h,
        self.nivel_l,
        self.Solenoide1,
        self.Solenoide2,
        self.Solenoide3,
        ]