import random
from datetime import datetime
from selectors import SelectorKey
from sqlite3 import Timestamp
from threading import Lock, Thread
from time import sleep

from kivy.clock import Clock
from kivy.core.window import Window
from kivy.properties import ListProperty
from kivy.uix.boxlayout import BoxLayout
from pyModbusTCP.client import ModbusClient

from popups import (
    ControleInversorPopup,
    ControleMotorPopup,
    DataGraphPopup,
    HistGraphPopup,
    ModbusPopup,
    ScanPopup,
)
from timeseriesgraph import TimeSeriesGraph

"""from bdhandler import BDHandler"""
from sqlite3.dbapi2 import enable_callback_tracebacks

from kivy_garden.graph import LinePlot

from dbalchemy import Base, Session, engine
from models import DadosCLP


class MainWidget(BoxLayout):

    _updateThread = None
    _updateWidgets = True
    _bandaHist = 500
    _setpointHist = 9500
    _enableHist = True
    _tags = {}
    _colorH = ListProperty((0.5, 0.5, 0.5, 1))
    _colorL = ListProperty((0.5, 0.5, 0.5, 1))

    def __init__(self, **kwargs):
        """Construtor do widget principal"""
        super().__init__()
        self._scan_time = kwargs.get("scan_time")
        self._serverIP = kwargs.get("server_ip")
        self._serverPort = kwargs.get("server_port")
        self._modbusPopup = ModbusPopup(self._serverIP, self._serverPort)
        self._scanPopup = ScanPopup(self._scan_time)
        self._modbusClient = ModbusClient(host=self._serverIP, port=self._serverPort)
        self._controleMotorPopup = ControleMotorPopup()
        self._controleInversorPopup = ControleInversorPopup()
        self._meas = {}
        self._meas["timestamp"] = None
        self._meas["values"] = {}
        for key, value in kwargs.get("modbus_addrs").items():
            if key == "nivel":
                plot_color = (0, 0, 1, 1)
            elif key == "vz_entrada":
                plot_color = (1, 0, 1, 1)
            else:
                plot_color = (random.random(), random.random(), random.random(), 1)
            self._tags[key] = value
            self._tags[key].update({"color": plot_color})
        self._graph = DataGraphPopup(
            self._tags["nivel"]["color"], self._tags["vz_entrada"]["color"]
        )
        self._hgraph = HistGraphPopup(tags=self._tags)

        self._session = Session()
        Base.metadata.create_all(engine)
        self._lock = Lock()
        self._lock_modbus = Lock()

    def cor_status_bomba(self, dt):
        """
        Altera cor status (on/off) da bomba
        """
        if self._meas["values"]["estado_mot"]:
            self.ids.img_bomba.source = "imagss/bombaverde.png"
        else:
            self.ids.img_bomba.source = "imagss/bombavermelho.png"

    def cor_status_solenoide_1(self, dt):
        """
        Altera cor status (on/off) do solenoide 1
        """
        if self._meas["values"]["Solenoide1"]:
            self.ids.img_solen1.source = "imagss/solenoideverde.png"
        else:
            self.ids.img_solen1.source = "imagss/solenoidevermelho.png"

    def cor_status_solenoide_2(self, dt):
        """
        Altera cor status (on/off) do solenoide 2
        """
        if self._meas["values"]["Solenoide2"]:
            self.ids.img_solen2.source = "imagss/solenoideverde.png"
        else:
            self.ids.img_solen2.source = "imagss/solenoidevermelho.png"

    def cor_status_solenoide_3(self, dt):
        """
        Altera cor status (on/off) do solenoide 3
        """
        if self._meas["values"]["Solenoide3"]:
            self.ids.img_solen3.source = "imagss/solenoideverde.png"
        else:
            self.ids.img_solen3.source = "imagss/solenoidevermelho.png"

    def startDataRead(self, ip, port):
        """
        Conecta com o servidor modbus
        """
        self._serverIP = ip
        self._serverPort = port
        self._modbusClient.host = self._serverIP
        self._modbusClient.port = self._serverPort
        try:
            Window.set_system_cursor("wait")
            self._modbusClient.open()
            Window.set_system_cursor("arrow")
            if self._modbusClient.is_open:
                self._updateThread = Thread(target=self.updater)
                self._updateThread.start()
                self.ids.img_con.source = "imagss/conectado.png"
                self._modbusPopup.dismiss()
            else:
                self._modbusPopup.setInfo("Falha na conexão com o servidor")
        except Exception as e:
            print("Erro no startDataRead: ", e.args)

    def updater(self):
        """
        Atualiza informações da interface
        """
        try:
            while self._updateWidgets:
                self._lock.acquire()
                self.readData()
                self.updateGUI()
                self.histerese_motor()
                self._lock.release()
                dado = DadosCLP(
                    timestamp=self._meas["timestamp"], **self._meas["values"]
                )
                self._lock.acquire()
                self._session.add(dado)
                self._session.commit()
                self._lock.release()
                sleep(self._scan_time / 1000)
        except Exception as e:
            self._modbusClient.close()
            print("Erro no updater: ", e.args)

    def readData(self):
        """Método para a leitura dos dados por meio do protocolo MODBUS"""
        self._meas["timestamp"] = datetime.now()
        try:
            for key, value in self._tags.items():
                if value["tipo"] >= 2:
                    self._lock_modbus.acquire()
                    self._meas["values"][key] = (
                        self._modbusClient.read_holding_registers(value["addr"], 1)[0]
                        / value["mult"]
                    )
                    self._lock_modbus.release()
                elif value["tipo"] <= 1:
                    self._lock_modbus.acquire()
                    self._meas["values"][key] = self._modbusClient.read_coils(
                        value["addr"], 1
                    )[0]
                    self._lock_modbus.release()
        except Exception as e:
            self._modbusClient.close()
            print("Erro no readData: ", e.args)

    def updateGUI(self):
        """
        Atualiza a interface beaseado nas informações vindas do modbus
        """
        try:
            for key, _ in self._tags.items():

                if key == "freq_mot":
                    self._controleMotorPopup.printa_freq_mot(self._meas["values"][key])

                if key == "rotacao":
                    self._controleMotorPopup.printa_rotacao(self._meas["values"][key])

                if key == "temp_estator":
                    self._controleMotorPopup.printa_tempEstator(
                        self._meas["values"][key]
                    )

                if key == "tensao":
                    self._controleInversorPopup.printa_tensao(self._meas["values"][key])

                if key == "pot_entrada":
                    self._controleInversorPopup.printa_pot_entrada(
                        self._meas["values"][key]
                    )

                if key == "corrente":
                    self._controleInversorPopup.printa_corrente(
                        self._meas["values"][key]
                    )

                if key == "estado_mot":
                    Clock.schedule_once(self.cor_status_bomba)

                if key == "nivel":
                    self.ids.nivel.text = str(self._meas["values"][key]) + " L"
                    self.ids.lb_nivel.size = (220, self._meas["values"][key] / 50)

                if key == "Solenoide1":
                    Clock.schedule_once(self.cor_status_solenoide_1)

                if key == "Solenoide2":
                    Clock.schedule_once(self.cor_status_solenoide_2)

                if key == "Solenoide3":
                    Clock.schedule_once(self.cor_status_solenoide_3)

                if key == "nivel_h":
                    if self._meas["values"][key]:

                        self._colorH = (1, 0, 0)
                    else:
                        self._colorH = (0, 1, 1)

                if key == "nivel_l":
                    if self._meas["values"][key]:
                        self._colorL = (0, 1, 1)
                    else:
                        self._colorL = (1, 0, 0)

        except Exception as e:
            self._modbusClient.close()
            print("Erro no updateGui: ", e.args)

        self._graph.ids.graph_vazao.updateGraph(
            (self._meas["timestamp"], self._meas["values"]["vz_entrada"]), 0
        )
        self._graph.ids.graph_nivel.updateGraph(
            (self._meas["timestamp"], self._meas["values"]["nivel"]), 0
        )

    def stopRefresh(self):
        """
        Para a atualização dos widgets (usado em módulos externos)
        """
        self._updateWidgets = False

    def getDataDB(self):
        """Método que coleta as informações da interface fornecidas pelo usuário e requisita a busca no BD"""
        try:
            init_t = self._hgraph.ids.txt_init_time.text
            print(f"{type(init_t)=}")
            final_t = self._hgraph.ids.txt_final_time.text
            print(f"{type(final_t)=}")
            cols = []
            for sensor in self._hgraph.ids.sensores.children:
                if sensor.ids.checkbox.active:
                    print(f"{type(sensor.id)=}")
                    cols.append(sensor.id)

            if init_t is None or final_t is None or len(cols) == 0:
                return

            cols.append("timestamp")

            self._lock.acquire()
            dados = self._session.query(DadosCLP).filter(
                DadosCLP.timestamp.between(init_t, final_t)
            )
            self._lock.release()
            print(f"{type(dados)=}")
            print(f"{dados=}")
            (
                dado_id,
                dado_ts,
                dado_estado_mot,
                dado_freq_des,
                dado_t_part,
                dado_freq_mot,
                dado_tensao,
                dado_rotacao,
                dado_pot_entrada,
                dado_corrente,
                dado_temp_estator,
                dado_vz_entrada,
                dado_nivel,
                dado_nivel_h,
                dado_nivel_l,
                dado_Solenoide1,
                dado_Solenoide2,
                dado_Solenoide3,
            ) = dados.get_attr_printable_list()
            print(f"{dado_ts=}")
            print(f"{len(dado_ts)=}")
            # print(f"{type(len(dados['timestamp']))=}")
            # print(f"{len(dados['timestamp'])=}")
            if dados is None or len(dados["timestamp"]) == 0:
                return

            self._hgraph.ids.graph.clearPlots()

            for key, value in dados.items():
                if key == "timestamp":
                    continue
                print(f"{type(key)=}")
                print(f"{type(value)=}")

                p = LinePlot(line_width=1.5, color=self._tags[key]["color"])
                p.points = [(x, value[x]) for x in range(0, len(value))]
                self._hgraph.ids.graph.add_plot(p)
            self._hgraph.ids.graph.xmax = len(dados[cols[0]])
            self._hgraph.ids.graph.update_x_labels(
                [
                    datetime.strptime(x, "%Y-%m-%d %H:%M:%S.%f")
                    for x in dados["timestamp"]
                ]
            )
        except Exception as e:
            print("Erro no getDataDB: ", e.args)

    def acao_tag(self, txt_input):
        try:
            if self._modbusClient.is_open:
                if self._meas["values"][txt_input]:
                    self._lock_modbus.acquire()
                    self._modbusClient.write_single_coil(
                        self._tags[txt_input]["addr"], False
                    )
                    self._meas["values"][txt_input] = self._modbusClient.read_coils(
                        self._tags[txt_input]["addr"], 1
                    )[0]
                    self._lock_modbus.release()
                    self.ids[txt_input].text = ("Ligar ") + str(
                        self.ids[txt_input].text
                    ).split(" ")[1]
                else:
                    self._lock_modbus.acquire()
                    self._modbusClient.write_single_coil(
                        self._tags[txt_input]["addr"], True
                    )
                    self._meas["values"][txt_input] = self._modbusClient.read_coils(
                        self._tags[txt_input]["addr"], 1
                    )[0]
                    self._lock_modbus.release()
                    self.ids[txt_input].text = ("Desligar ") + str(
                        self.ids[txt_input].text
                    ).split(" ")[1]
            else:
                self._modbusPopup.setInfo("Falha na conexão com o servidor")
        except Exception as e:
            print("Erro no acao_tag: ", e.args)

    def acao_valor_tag(self, txt_input, valor):
        try:
            if self._modbusClient.is_open:
                self._lock_modbus.acquire()
                self._modbusClient.write_single_register(
                    self._tags[txt_input]["addr"], valor
                )
                self._meas["values"][
                    txt_input
                ] = self._modbusClient.read_input_registers(
                    self._tags[txt_input]["addr"], 1
                )[
                    0
                ]
                self._lock_modbus.release()

            else:
                self._modbusPopup.setInfo("Falha na conexão com o servidor")
        except Exception as e:
            print("Erro no acao_valor_tag: ", e.args)

    def histerese_motor(self):
        print(self._setpointHist + (self._bandaHist / 2))
        if self._enableHist:
            if self._meas["values"]["nivel"] > (
                self._setpointHist + (self._bandaHist / 2)
            ):
                self.acao_tag("estado_mot")
            elif self._meas["values"]["nivel"] < (
                self._setpointHist - (self._bandaHist / 2)
            ):
                if self._meas["values"]["estado_mot"] == False:
                    self.acao_tag("estado_mot")

    def change_enable_histerese(self):
        if self._enableHist:
            self.ids.botao_histerese.text = str("Ligar")
            self.ids.enable_histereses.color = (1, 0, 0)
            self._enableHist = False
        else:
            self.ids.botao_histerese.text = str("Desligar")
            self.ids.enable_histereses.color = (0, 1, 0)
            self._enableHist = True

    def change_valores_histerese(self, ident):
        if ident == 0:
            self._setpointHist = int(self.ids.txt_set_point.text)
        else:
            self._bandaHist = int(self.ids.txt_banda_hist.text)
