from kivy.app import App
from kivy.lang.builder import Builder

from mainwidget import MainWidget


class MainApp(App):
    def build(self):
        """addr: [tipo(1=coil,2=input registers)]"""
        self._widget = MainWidget(scan_time=1000,server_ip='127.0.0.1',server_port=502,
        modbus_addrs = {
            'estado_mot': {'tipo':1,'addr':800,'mult':None},
            'freq_des': {'tipo':3,'addr':799,'mult':1},
            't_part': {'tipo':3,'addr':798,'mult':10},
            'freq_mot': {'tipo':2,'addr':800,'mult':10},
            'tensao': {'tipo':2,'addr':801,'mult':1},
            'rotacao': {'tipo':2,'addr':803,'mult':1},
            'pot_entrada': {'tipo':2,'addr':804,'mult':10},
            'corrente': {'tipo':2,'addr':805,'mult':100},
            'temp_estator': {'tipo':2,'addr':806,'mult':10},
            'vz_entrada': {'tipo':2,'addr':807,'mult':100},
            'nivel':{'tipo':2,'addr':808,'mult':10},
            'nivel_h':{'tipo':0,'addr':809,'mult':None},
            'nivel_l':{'tipo':0,'addr':810,'mult':None},
            'Solenoide1':{'tipo':1,'addr':801,'mult':None},
            'Solenoide2':{'tipo':1,'addr':802,'mult':None},
            'Solenoide3':{'tipo':1,'addr':803,'mult':None},
        },
        db_path="C:\\Users\\marmo\\Documents\\InformaticaIndustrial\\TrabalhoFinal\\Programa\\db\\scada.db"
        )
        return self._widget
    def on_stop(self):
        """Método executado quando a aplicação é fechada"""
        self._widget.stopRefresh()

if __name__ == '__main__':
    Builder.load_string(open("mainwidget.kv",encoding="utf-8").read(),rulesonly=True)
    Builder.load_string(open("popups.kv",encoding="utf-8").read(),rulesonly=True)
    MainApp().run()
